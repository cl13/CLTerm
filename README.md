# CLTerm
## A Terminal emulator for Linux
Logo
<p align="center">
    <img width="400" alt="CLTerm Logo" src="Assets/CLTerm.png">
</p>
Pictures
<p align="center">
    <img width="400" alt="CLTerm Pictures" src="Assets/neofetch.png">
</p>
Welcome Screen
<p>
	<img width="400" alt="CLTerm Welcome Screen" src="Assets/Welcome_Screen.png">
</p>

# Requirements
``gtk-3.0``
``vte``
``g++``
``make``

# Install
First install the dependencies above. Then:

``git clone https://gitlab.com/cl13/CLash.git``

``cd CLash``

``make install``

``cd ..``

``git clone https://gitlab.com/cl13/CLTerm.git``

``cd CLTerm``

``make install``

# What is it?
CLTerm is a terminal emulator for linux made with GTK 3.0. It runs a shell called CLash.
