#include "term.hh"


Terminal::Terminal()
{
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	terminal = vte_terminal_new();
}

void Terminal::InitBorder(int x, int y)
{
  gtk_widget_style_get (this->terminal, "inner-border", &this->border, NULL);
  gtk_window_set_default_geometry(GTK_WINDOW(this->window),
   	x * vte_terminal_get_char_width(VTE_TERMINAL(this->terminal)) + 
   	(this->border ? (this->border->left + this->border->right) : 0),
   	y * vte_terminal_get_char_height(VTE_TERMINAL(this->terminal)) + 
   	(this->border ? (this->border->top + this->border->bottom) : 0));
  return;
}

void Terminal::InitTerminal(char *shell)
{
  g_shell_parse_argv(shell, 0, &this->__shell__, 0);
  vte_terminal_spawn_sync(VTE_TERMINAL(this->terminal), VTE_PTY_DEFAULT, NULL,
 	 &shell, NULL, G_SPAWN_DO_NOT_REAP_CHILD, NULL, NULL, NULL, NULL, NULL);  

  return;
}

void Terminal::DestroySignal()
{
  g_signal_connect(this->window, "delete_event", gtk_main_quit, NULL);
  g_signal_connect(this->window, "child-exited", gtk_main_quit, NULL);
  return;
}
