#ifndef __CLXEMUL_H__
#define __CLXEMUL_H__

#include <vte/vte.h>

class Terminal
{
public:
	Terminal(); // ctor

	// Init Methods
	void InitBorder(int, int);
	void InitTerminal(char *shell);

	// Destroy Methods	
	void DestroySignal(void);
	
	// Objects
	GtkWidget *window;
	GtkWidget *terminal;
	GtkBorder *border;	
	char **__shell__ = 0;
};
#endif
