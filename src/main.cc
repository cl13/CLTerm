#include "term.hh"
#include "keys.hh"

int main(int argc, char **argv)
{
  gtk_init(&argc, &argv);

  Terminal terminal;

#if GTK_CHECK_VERSION(2,91,1)
  gtk_window_set_has_resize_grip(GTK_WINDOW(terminal.window), 0);
#endif
  char *shell = "/usr/bin/CLash";
  terminal.InitTerminal(shell);
  gtk_container_add(GTK_CONTAINER(terminal.window), terminal.terminal);

#if VTE_CHECK_VERSION(0,27,1) && GTK_CHECK_VERSION(2,91,1)
  terminal.InitBorder(80, 24);
#endif

  gtk_widget_show_all(terminal.window);

#ifdef NORMAL_BEHAVIOR
  terminal.DestroySignal();
#endif

  gtk_main();

  return 0;
}
