from os.path import expanduser

home = expanduser("~")
_icon = home + "/.local/share/applications/CLTerm/CLTerm.png"
_exec = home + "/.local/share/applications/CLTerm/CLTerm"
_desktop = home + "/.local/share/applications/CLTerm.desktop"

desktop_file = open(_desktop, "a")

write_exec_path = "Exec=" + _exec
write_icon_path = "Icon=" + _icon

desktop_file.write(write_exec_path)
desktop_file.write("\n")
desktop_file.write(write_icon_path)
desktop_file.write("\n")

