if [[ ! -d ~/.config/CLTerm ]]
then
	mkdir ~/.config/CLTerm 
fi
if [[ ! -f ~/.config/CLTerm/config.cluu ]]
then
    cp ./config.cluu ~/.config/CLTerm/
fi
if [[ -f ~/.config/CLTerm/config.cluu ]]
then 
    echo "Config exists. Not copying default config."
fi
