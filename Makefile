build:
	cd src/ && sh build 

install: build
	mkdir -p ~/.local/share/applications/CLTerm
	cp ./CLTerm.desktop ~/.local/share/applications/CLTerm.desktop
	cp ./src/bin/CLTerm ~/.local/share/applications/CLTerm/CLTerm
	cp ./Assets/CLTerm.png ~/.local/share/applications/CLTerm/CLTerm.png
	python3 config_desktop.py
	sudo cp ./src/bin/CLTerm /usr/bin/CLTerm
	sh add_config.sh

clean_CLTerm:
	rm -r ~/.local/share/applications/CLTerm
	sudo rm /usr/bin/CLTerm

uninstall: clean_CLTerm
	
reinstall: clean_CLTerm build install
